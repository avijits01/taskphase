import cv2
import numpy as np
import math
#reading the images and converting them to grayscale for better disparity calculations

#reading the images and converting them to grayscale for better disparity calculations
leftI=cv2.cvtColor(cv2.imread('right.png'),cv2.COLOR_BGR2GRAY)
rightI=cv2.cvtColor(cv2.imread('left.png'),cv2.COLOR_BGR2GRAY)
#converting the images to grayscale
# imgL=cv2.
disparityrange=50
height,width= leftI.shape
halfblock,block=3,7
Dbasic=np.zeros(leftI.shape,dtype=np.float32)
for m in range(0,height):
    #set min/max row bounds for the template and blocks
    minr=max([0,m-halfblock])
    maxr=min([height,m+halfblock])
    for n in range(0,width):
        minc=max([0,n-halfblock])
        maxc=min([width,n+halfblock])
        # Define the search boundaries as offsets from the template location.
    	# Limit the search so that we don't go outside of the image.
    	# 'mind' is the the maximum number of pixels we can search to the left.
    	# 'maxd' is the maximum number of pixels we can search to the right.
    	# In the "Cones" dataset, we only need to search to the right, so mind
    	# is 0.
    	# For other images which require searching in both directions, set mind
    	# as follows:
        # mind = max(range(-disparityrange, 1 - minc))
        mind=0
        maxd=min([disparityrange,width-maxc])
        template=rightI[minr:maxr,minc:maxc]
        numblocks=maxd-mind+1
        blockDiffs=np.zeros((numblocks))
        MINIMUM_INDEX=0
        MINIMUM_SAD=100000000000000000
        for i in range(mind,maxd+1):
            block=leftI[minr:maxr,minc+i:maxc+i]
            blockindex=i-mind
            SAD=np.sum(np.absolute(template-block))
            if(SAD<MINIMUM_SAD):
                MINIMUM_SAD=SAD
                MINIMUM_INDEX=blockindex
        disparity=MINIMUM_INDEX
        Dbasic[m,n]=disparity
print(Dbasic)\\\\
