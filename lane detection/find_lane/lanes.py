import cv2
import numpy as np
import matplotlib.pyplot as plt
def make_coordinates(image,line_parameters):
    slope,intercept=line_parameters
    y1=image.shape[0] #image.shape has three datas. Bottom or height of the image, width of the image, number of chanels. And the height is actually the bottom coordinate so vala.
    y2=int(y1*3/5) #this signifies that the second coordinate will get 3/5th above the image.
    x1=int((y1-intercept)/slope) #comes from making x independent from the equation y=mx+c
    x2=int((y2-intercept)/slope)
    return np.array([x1,y1,x2,y2])
def convert_hls(image):
    return cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
def select_white_yellow(image):
    converted = convert_hls(image)
    # white color mask
    lower = np.uint8([  0, 200,   0])
    upper = np.uint8([255, 255, 255])
    white_mask = cv2.inRange(converted, lower, upper)
    # yellow color mask
    lower = np.uint8([ 10,   0, 100])
    upper = np.uint8([ 40, 255, 255])
    yellow_mask = cv2.inRange(converted, lower, upper)
    # combine the mask
    mask = cv2.bitwise_or(white_mask, yellow_mask)
    return cv2.bitwise_and(image, image, mask = mask)
def select_rgb_white_yellow(image):
    image=cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
    lower = np.uint8([200, 200, 200])
    upper = np.uint8([255, 255, 255])
    white_mask = cv2.inRange(image, lower, upper)
    lower = np.uint8([190, 190,   0])
    upper = np.uint8([255, 255, 255])
    yellow_mask = cv2.inRange(image, lower, upper)
    mask = cv2.bitwise_or(white_mask, yellow_mask)
    masked = cv2.bitwise_and(image, image, mask = mask)
    return mask
def filter_colors_hsv(img):
    """
    Convert image to HSV color space and suppress any colors
    outside of the defined color ranges
    """
    img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    yellow_dark = np.array([20, 100, 100], dtype=np.uint8)
    yellow_light = np.array([30, 255, 255], dtype=np.uint8)
    yellow_range = cv2.inRange(img, yellow_dark, yellow_light)
    white_dark = np.array([0, 0, 200], dtype=np.uint8)
    white_light = np.array([255, 30, 255], dtype=np.uint8)
    white_range = cv2.inRange(img, white_dark, white_light)
    yellows_or_whites = cv2.bitwise_or(yellow_range,white_range)
    img = cv2.bitwise_and(img, img, mask=yellows_or_whites)
    return img
def to_hsv(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
def to_hsl(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
def average_slope(image,lines):
    left_fit = []
    right_fit = []
    for line in lines:
        x1,y1,x2,y2=line.reshape(4)
        parameters=np.polyfit((x1,x2),(y1,y2),1) #fits a polynomial of degree [3rd argument], returns slope and y intercept
        slope = parameters[0]
        intercept=parameters[1]
        if slope <0:
            left_fit.append((slope,intercept))
        else:
            right_fit.append((slope,intercept))
    left_fit_average=np.average(left_fit,axis=0)
    right_fit_average=np.average(right_fit,axis=0)
    left_coordinates=make_coordinates(image,left_fit_average)
    right_coordinates=make_coordinates(image,right_fit_average)

    return np.array([left_coordinates,right_coordinates])
def colour_saturation(image):
    exit()

def canny(image):
    gray = filter_colors_hsv(image)

    blur = cv2.GaussianBlur(gray,(5,5),0)
    canny=cv2.Canny(blur,50,150)
    return gray
def region_of_interest(image):
        height=image.shape[0]
        polygons=np.array([[(150,height),(1150,height),(680,473)]],dtype=np.int32)
        mask=np.zeros_like(image)
        cv2.fillPoly(mask,polygons,255)
        masked_image=cv2.bitwise_and(image, mask)
        return masked_image
def warper(img):

    src = np.float32([
        [150, 720],
        [590, 450],
        [690, 450],
        [1150, 720]
    ])
    dst = np.float32([
        [250, 720],
        [250, 0],
        [1035, 0],
        [1035, 720]
    ])

    # Compute and apply perpective transform
    img_size = (img.shape[1], img.shape[0])
    M = cv2.getPerspectiveTransform(src, dst)
    warped = cv2.warpPerspective(img, M, img_size, flags=cv2.INTER_NEAREST)  # keep same size as input image

    return warped
# def binaryPipe(img):
#
#     # Copy the image
#     img = np.copy(img)
#
#     # Undistort the image based on the camera calibration
#     undist = undistort(img, mtx, dist)
#
#     # warp the image based on our perspective transform
#     warped = warper(undist)
# 
#     # Get the Red and saturation images
#     red = rgb_rthresh(warped, thresh=(225, 255))
#
#     # Run the sobel magnitude calculation
#     sobel = calc_sobel(warped, sobel_kernel=15,  mag_thresh=(50, 220))
#
#     # combine these layers
#     combined_binary = np.zeros_like(sobel)
#     combined_binary[ (red == 1) | (sobel == 1) ] = 1
#
#     return combined_binary
def display_line(image,lines):
            line_image=np.zeros_like(image)
            if lines is not None:
                for line in lines:
                    x1,y1,x2,y2=line.reshape(4)
                    cv2.line(line_image,(x1,y1),(x2,y2),(255,0,0),10)
            return line_image
# image = cv2.imread('test_image.jpg')
# lane_image=np.copy(image)
# canny_image=canny(lane_image)
# cropped_image=region_of_interest(canny_image)
# lines = cv2.HoughLinesP(cropped_image,2,np.pi/180,100,np.array([]),minLineLength=40,maxLineGap=5)
# average_lines=average_slope(image,lines)
# line_image=display_line(lane_image,average_lines)
# mix_image=cv2.addWeighted(lane_image,0.8,line_image,1,1)
# cv2.imshow("result",mix_image)
# cv2.waitKey(0)
cap=cv2.VideoCapture("challenge_video.mp4")
while(cap.isOpened()):
    _ , frame=cap.read()
    canny_image=select_white_yellow(frame)
    # plt.imshow(frame)
    # plt.show()
    # hsv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # cropped_image=region_of_interest(canny_image)
    # lines = cv2.HoughLinesP(cropped_image,2,np.pi/180,50,np.array([]),minLineLength=20,maxLineGap=100)
    # average_lines=average_slope(frame,lines)
    # line_image=display_line(frame,lines)
    # mix_image=cv2.addWeighted(frame,0.8,line_image,1,1)
    cv2.imshow("result",canny_image)
    cv2.waitKey(0)
